package com.pawelbanasik.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LuvAopExpressions {

	@Pointcut("execution(* com.pawelbanasik.dao.*.*(..))")
	public void forDaoPackage() {}
	
	@Pointcut("execution(* com.pawelbanasik.dao.*.get*(..))")
	public void getter() {}
	
	@Pointcut("execution(* com.pawelbanasik.dao.*.set*(..))")
	public void setter() {}
	
	@Pointcut("forDaoPackage() && !(getter() || setter())")
	public void forDaoPackageNoGetterSetter() {}
	
	
}
